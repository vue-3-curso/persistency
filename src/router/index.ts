import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import WatchEffectCompositionApi from '@/components/WatchEffectCompositionApi.vue' 
import WatchersOptionApi from '@/components/WatchersOptionnApi.vue'
import WatchersCompositionApi from '@/components/WatchersCompositionApi.vue'
import PiniaPersistedState  from '@/components/PiniaPersistedState.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'pinia-pe',
    component: PiniaPersistedState
  },
  {
    path: '/watch-effect-ca',
    name: 'watcheffect-ca',
    component: WatchEffectCompositionApi
  },
  {
    path: '/watchers-ca',
    name: 'watchers-ca',
    component: WatchersCompositionApi
  },
  {
    path: '/watchers-oa',
    name: 'watchers-oa',
    component: WatchersOptionApi
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
