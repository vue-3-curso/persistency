# persistency

Para LocalStorage:
~~~~
export  const  useExample=defineStore('useExample',{
    state:()=>{
        return {count:1}
        
    },
    actions:{
        init(){
            const initCount=localStorage.getItem('count')
            if (initCount){
                this.count=parseInt(initCount)
            }
        }, 
        increment(val=1){
            this.count=this.count+val
            localStorage.setItem('count',this.count.toString())
        }
    }
}
)
~~~~~

Y se llamaría la función init() desde el componente.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
